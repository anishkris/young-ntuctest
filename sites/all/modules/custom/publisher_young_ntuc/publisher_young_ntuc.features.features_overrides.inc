<?php
/**
 * @file
 * publisher_young_ntuc.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function publisher_young_ntuc_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: fe_block_settings
  $overrides["fe_block_settings.block-dashboard_block.themes|bartik"]["DELETED"] = TRUE;
  $overrides["fe_block_settings.block-dashboard_block.themes|publisher_base"] = array(
    'region' => '',
    'status' => 0,
    'theme' => 'publisher_base',
    'weight' => 0,
  );
  $overrides["fe_block_settings.menu-menu-manage-posts.themes|bartik"]["DELETED"] = TRUE;
  $overrides["fe_block_settings.menu-menu-manage-posts.themes|publisher_base"] = array(
    'region' => '',
    'status' => 0,
    'theme' => 'publisher_base',
    'weight' => 0,
  );
  $overrides["fe_block_settings.menu-menu-webmaster.themes|bartik"]["DELETED"] = TRUE;
  $overrides["fe_block_settings.menu-menu-webmaster.themes|publisher_base"] = array(
    'region' => '',
    'status' => 0,
    'theme' => 'publisher_base',
    'weight' => 0,
  );

  // Exported overrides for: field_instance
  $overrides["field_instance.node-article-field_category_single.required"] = 0;
  $overrides["field_instance.node-events-field_category_single.required"] = 0;
  $overrides["field_instance.node-profile-field_category_single.required"] = 0;

  // Exported overrides for: variable
  $overrides["variable.pathauto_node_article_pattern.value"] = 'articles/[node:title]';
  $overrides["variable.pathauto_node_events_pattern.value"] = 'events/[node:title]';
  $overrides["variable.pathauto_node_profile_pattern.value"] = 'unionist-profiles/[node:title]';

  // Exported overrides for: views_view
  $overrides["views_view.publisher_profile.display|default|display_options|title"] = 'Activists in  Action';

 return $overrides;
}

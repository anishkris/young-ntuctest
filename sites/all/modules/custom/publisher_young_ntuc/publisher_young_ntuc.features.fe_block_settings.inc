<?php
/**
 * @file
 * publisher_young_ntuc.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function publisher_young_ntuc_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-publisher_article-front_block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'publisher_article-front_block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'publisher_base' => array(
        'region' => 'front_bottom',
        'status' => 1,
        'theme' => 'publisher_base',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-publisher_events-front_block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'publisher_events-front_block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'publisher_base' => array(
        'region' => 'front_bottom',
        'status' => 1,
        'theme' => 'publisher_base',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-publisher_profile-front_block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'publisher_profile-front_block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'publisher_base' => array(
        'region' => 'front_top',
        'status' => 1,
        'theme' => 'publisher_base',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}

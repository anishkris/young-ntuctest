<?php
/**
 * @file
 * publisher_young_ntuc.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function publisher_young_ntuc_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'theme_default';
  $strongarm->value = 'publisher_base';
  $export['theme_default'] = $strongarm;

  return $export;
}

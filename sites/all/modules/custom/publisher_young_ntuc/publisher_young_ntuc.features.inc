<?php
/**
 * @file
 * publisher_young_ntuc.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function publisher_young_ntuc_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_fe_block_settings_alter().
 */
function publisher_young_ntuc_default_fe_block_settings_alter(&$data) {
  if (isset($data['block-dashboard_block'])) {
    $data['block-dashboard_block']['themes']['publisher_base'] = array(
      'region' => '',
      'status' => 0,
      'theme' => 'publisher_base',
      'weight' => 0,
    ); /* WAS: '' */
    unset($data['block-dashboard_block']['themes']['bartik']);
  }
  if (isset($data['menu-menu-manage-posts'])) {
    $data['menu-menu-manage-posts']['themes']['publisher_base'] = array(
      'region' => '',
      'status' => 0,
      'theme' => 'publisher_base',
      'weight' => 0,
    ); /* WAS: '' */
    unset($data['menu-menu-manage-posts']['themes']['bartik']);
  }
  if (isset($data['menu-menu-webmaster'])) {
    $data['menu-menu-webmaster']['themes']['publisher_base'] = array(
      'region' => '',
      'status' => 0,
      'theme' => 'publisher_base',
      'weight' => 0,
    ); /* WAS: '' */
    unset($data['menu-menu-webmaster']['themes']['bartik']);
  }
}

/**
 * Implements hook_field_default_field_instances_alter().
 */
function publisher_young_ntuc_field_default_field_instances_alter(&$data) {
  if (isset($data['node-article-field_category_single'])) {
    $data['node-article-field_category_single']['required'] = 0; /* WAS: 1 */
  }
  if (isset($data['node-events-field_category_single'])) {
    $data['node-events-field_category_single']['required'] = 0; /* WAS: 1 */
  }
  if (isset($data['node-profile-field_category_single'])) {
    $data['node-profile-field_category_single']['required'] = 0; /* WAS: 1 */
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function publisher_young_ntuc_strongarm_alter(&$data) {
  if (isset($data['pathauto_node_article_pattern'])) {
    $data['pathauto_node_article_pattern']->value = 'articles/[node:title]'; /* WAS: '[node:field-category-single]/[node:title]' */
  }
  if (isset($data['pathauto_node_events_pattern'])) {
    $data['pathauto_node_events_pattern']->value = 'events/[node:title]'; /* WAS: '[node:field-category-single]/[node:title]' */
  }
  if (isset($data['pathauto_node_profile_pattern'])) {
    $data['pathauto_node_profile_pattern']->value = 'unionist-profiles/[node:title]'; /* WAS: '[node:field-category-single]/[node:title]' */
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function publisher_young_ntuc_views_default_views_alter(&$data) {
  if (isset($data['publisher_profile'])) {
    $data['publisher_profile']->display['default']->display_options['title'] = 'Activists in  Action'; /* WAS: 'Profiles' */
  }
}

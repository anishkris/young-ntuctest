<?php
  hide($content['links']);
  hide($content['field_content_media']);
  hide($content['field_content_title']);
  hide($content['field_content_caption']);
  hide($content['field_content_livestream']);
?>

		<div class="single-article <?php print $classes; ?>">
		
		<figure class="standfirst">
			<?php //print publisher_render_field_single('node', $node, 'field_featured_image');?>
      <?php if(isset($content_top)) print $content_top; ?>
    </figure>
		
		<h2 class="content-title">
      <!--span class="title"-->
        <?php print publisher_render_field_single('node', $node, 'field_content_title'); ?>
      <!--/span-->
    </h2>
        
         <?php print publisher_render_field_single('node', $node, 'body');?>
        
    	<div class="group">
        	<dl class="events">
            	<dt class="">Duration:</dt>
                <dd>
            	<?php 
				$date_start = publisher_render_field('node', $node, 'field_start_date');
				$date_end = publisher_render_field('node', $node, 'field_end_date');
				print date("d M Y",strtotime($date_start[0]['value']));
				print " - ";
				print date("d M Y",strtotime($date_end[0]['value']));?>
            	</dd>
                
                <dt>Time:</dt>
                <dd>
                <?php print publisher_render_field('node', $node, 'field_time_collection',"datetime");?>
                </dd>
                
                <dt>Fees:</dt>
                <dd><?php 
				 print publisher_render_field('node', $node, NULL,"fees","events");?>
                </dd>
                
                <dt>Venue:</dt>
                <dd><?php 
				 print publisher_render_field('node', $node, NULL,"building");?>
		    	</dd>
                
            </dl>
           </div><!-- end group -->
        
         <?php print render($content['field_event_note']);?>

		<?php print render($content['field_terms_and_conditions']);?>

		<div class="brand-logo group">
			<?php print render($content['field_organised_by']);?>
		</div><!-- brand-logo -->
		
		</div><!-- end single-article -->
	
	<?php //include 'sidebar.php';?>
	

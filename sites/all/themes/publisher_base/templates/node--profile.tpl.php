<?php
  hide($content['links']);
  hide($content['field_content_media']);
  hide($content['field_content_title']);
  hide($content['field_featured_caption']);
  hide($content['field_content_livestream']);
?>
<div class="single-article <?php print $classes; ?>">
  <figure class="standfirst">
    <?php if(isset($content_top)) print $content_top; ?>
  </figure>
  <h2 class="content-title">
    <!--span class="title"-->
      <?php print publisher_render_field_single('node', $node, 'field_content_title'); ?>
    <!--/span-->
    <!--<span class="author">
      <?php //print ucwords(publisher_render_field_single('node', $node, 'field_content_contributor')); ?>
    </span>-->
  </h2>
  <p class="content-caption"><?php print render($content['field_featured_caption']); ?></p>
  <div class="content"><?php print render($content); ?></div>
</div>
